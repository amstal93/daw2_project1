const path = require('path');
const bodyParser = require('body-parser');
const express = require('express');
const session = require('express-session');
const cors = require('cors');
import { ApolloServer, gql } from "apollo-server-express";
import { ApolloGateway } from "@apollo/gateway";


async function main() {
  const app = express();


app.use(session({ secret: 'Babalan Pasalan', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false  }));

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
    const gateway = new ApolloGateway({
        serviceList: [
          { name: 'Shops', url: 'http://localhost:4000/graphql' },
          { name: 'Products', url: 'http://localhost:4001/graphql' }
          // more services
        ],
      });
      
      // Pass the ApolloGateway to the ApolloServer constructor
      const server = new ApolloServer({
        gateway,
      
        // Disable subscriptions (not currently supported with ApolloGateway)
        subscriptions: false,
      });
    
      server.applyMiddleware({ app });
    
    app.listen({ port: 3999 }, () =>
        console.log(`🚀 Server ready at http://localhost:3999${server.graphqlPath}`)
    );
}
main();
