import { ApolloServer, gql } from "apollo-server-express";
import  { buildFederatedSchema } from '@apollo/federation';
import express from "express";

const startServer = async () => {
  const app = express();

  var mongoose = require('mongoose');
  const Shops_model = require('./models/Shops.model');
  let Shop = mongoose.model('shops'); 
  const Products_model = require('./models/Products.model');
  let Products = mongoose.model('products'); 
  await mongoose.connect("mongodb://localhost:27017/StarWarsTravel_test", {
    useNewUrlParser: true
  });
  
  const typeDefs = gql`
    extend type Query {
      all_shops: [Shop],
      shop(id:String!):Shop
    }
    extend type Product @key(fields: "product_id") {
      product_id: Int! @external
      name: String @external
      description: String @external
      image: String @external
      shop: [Shop]
      
    }
    type Shop @key(fields: "id") {
      id: ID!
      name:String
      stock:[Product] @provides(fields: "name,description,image")
      image:String
      products:[Int]  
    }
    
  `;
  let all_products_array;
  let all_shops_array;
  Products.find().then(function(result) {/* console.log(result); */  all_products_array= result })
  Shop.find().then(function(result) { all_shops_array= result })
  const resolvers = {
    Query: {
      all_shops() {
        return Shop.find()
      },
      shop: (root, {id}) => {
        return Shop.findById(id);
      },
    },
    Shop: {
      stock(review) {
        let object=[];
        review.products.forEach(product_id => {
          object.push({ __typename: "Product", product_id: product_id})
        });
        /* console.log(object) */
        return object ;
      }
    },
    Product: {
      name(user) {
       const found = all_products_array.find(names => names.product_id == user.product_id);
       return found.name;
      },
      description(user) {
        const found = all_products_array.find(names => names.product_id == user.product_id);
        return found.description;
      },
      image(user) {
        const found = all_products_array.find(names => names.product_id == user.product_id);
        return found.image;
      }, 
      shop(product) {
        let arrayTest = [];
       all_shops_array.forEach(element => {
         if(element.products.includes( product.product_id )){
           /* console.log(element.name + " " + product.product_id) */
           arrayTest.push(element)
         }
       });
        


        return arrayTest;
      }
     },
  }
  const server = new ApolloServer({
    schema: buildFederatedSchema([
      {
        typeDefs,
        resolvers,
      },
    ]),
  });

  server.applyMiddleware({ app });

  app.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
  );
};

startServer();
