const mongoose = require('mongoose');

const ShopsSchema = new mongoose.Schema({
  name: String,
  products: { type: Array, ref: 'Products' },
  image:String
}, {timestamps: true});

mongoose.model('shops', ShopsSchema);