const Questions = {
    categorias: ({ id }, args, context) => {
      return context.prisma.questions({ id }).categorias()
    },
    answer: ({ id }, args, context) => {
      return context.prisma.questions({ id }).answer()
    },
  }
  
  module.exports = {
    Questions,
  }