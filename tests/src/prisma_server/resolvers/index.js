const { Query,Mutation } = require('./Query')
const { Questions } = require('./Questions')
const { Game } = require('./Game')

const resolvers = {
  Query,
  Mutation,
  Questions,
  Game
}

module.exports = {
  resolvers,
}