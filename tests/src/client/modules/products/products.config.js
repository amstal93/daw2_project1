const products_graphql_query = require("../../services/graphql_services/products.querys");
const shops_graphql_query = require("../../services/graphql_services/shops.querys");
function PrductsConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
    .state('app.products', { //details of selected planet
      url: '/products/:slug',
      controller: 'PrductsCtrl',
      controllerAs: '$ctrl',
      template: require('./product.html'),
      title: 'Product',
      resolve: {
        planet: function(APIService, $state, $stateParams) {
            console.log($stateParams)
          return APIService.get_delete("planets/"+$stateParams.slug,"GET").then(
            (planet) => planet,
            (err) => $state.go('app.home')
          )
        }
      }
    }).state('app.shops', { //details of selected shop
      url: '/shop/:shop_id',
      controller: 'ShopsCtrl',
      controllerAs: '$ctrl',
      template: require('./shops/shops.html'),
      title: 'shops',
      resolve: {
        shop_id: function(GraphqlService, $state, $stateParams) {
          console.log($stateParams)
          let query = shops_graphql_query.shops_query.get_shop_by_id($stateParams.shop_id);
          return GraphqlService.get(query).then(function(shop) {
            return shop
          })
        }
      }
    }).state('app.all_products', { //list of all products
      url: '/all_products',
      controller: 'ShopProductsCtrl',
      controllerAs: '$ctrl',
      template: require('./products/list_products.html'),
      title: 'Product',
    }).state('app.single_product', { //details of selected product
      url: '/single_product/:product_id',
      controller: 'SingleProductsCtrl',
      controllerAs: '$ctrl',
      template: require('./products/product.details.html'),
      resolve: {
        product: async function( $state, $stateParams) {
          let query = products_graphql_query.poducts_query.get_product_by_productId($stateParams.product_id);
          return query;
            
        }
      }
    })
  
  };
  
  export default PrductsConfig;