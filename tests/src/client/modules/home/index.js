import angular from 'angular';

// Create the module where our functionality can attach to
let HomeModule = angular.module('app.home', []);

// Include our UI-Router config settings
import HomeConfig from './home.config';
HomeModule.config(HomeConfig);


// Controllers
import HomeCtrl from './home.controller';
HomeModule.controller('HomeCtrl', HomeCtrl);


export default HomeModule;