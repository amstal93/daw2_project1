const App = require("../config/constants.config");
function authInterceptor(JWT, $window, $q) {
    'ngInject'
  
    return {
      // automatically attach Authorization header
      request: function(config) {
        if(config.url.indexOf(App.AppConstants.api) === 0 && JWT.jwt_get()) {
          config.headers.Authorization = 'Token ' + JWT.jwt_get();
        }
        return config;
      },
  
      // Handle 401
      responseError: function(rejection) {
        if (rejection.status === 401) {
          // clear any JWT token being stored
          JWT.jwt_destroy();
          // do a hard page refresh
          $window.location.reload();
        }
        return $q.reject(rejection);
      }
  
    }
  }
  
  export default authInterceptor;