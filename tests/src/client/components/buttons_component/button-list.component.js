class ButtonListCtrl {
    constructor() {

    }
  
  }
  
  let ButtonList= {
    bindings: { 'filter': '=' },
    controller: ButtonListCtrl,
    controllerAs: 'ctrl',
    template: function($attrs) {
        'ngInject';
        switch ($attrs.filter) {
            case "share":/* only share button */
                return `<a class="btn-floating  blue"><i class="material-icons">share</i></a>`;
            case "profile":/* Profile */
                return ` <li class="nav-item"  show-authed="true"  id="test_profile_button">
                                <a class="nav-link" ui-sref="app.profile">Profile</a>
                            </li>`;
            case "profile_settings": /* Update Profile */
                return `<li show-authed="true" class="nav-item" id="test_profileSet_button">
                            <a  ui-sref="app.settings" class="btn-floating green"><i class="material-icons">settings</i></a>
                        </li>`;
            case "logout":/* Logout */
                return ` <li class="nav-item"  show-authed="true" id="test_logout_button">
                            <a class="nav-link"  ui-sref="app.logout">Log Out</a>
                        </li>`;
            case "login":/* Login */
                return ` <li class="nav-item" show-authed="false" id="test_login_button">
                            <a class="nav-link"  ui-sref="app.auth">Login </a>
                        </li>`;
            case "home":/* Home */
                return ` <li class="nav-item" id="test_home_button">
                            <a class="nav-link" ui-sref-active="active" ui-sref="app.home">Home</a>
                        </li>`
            case "contact":/* Contact Us */
                return ` <li class="nav-item" id="test_contact_button">
                            <a class="nav-link" ui-sref="app.contact"> Contact</a>
                        </li>`;
            case "quiz":/* Quiz */
                return ` <li class="nav-item" id="test_quiz_button">
                            <a class="nav-link" ui-sref="app.quiz"> Quiz Up</a>
                        </li>`;
            default:
                break;
        }
      }
  };

export default ButtonList;


