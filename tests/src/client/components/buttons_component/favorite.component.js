class FavoriteBtnCtrl {
    constructor(APIService, UserService, $state,$scope) {
      'ngInject';
      this.$onInit =function() {
          this._User = UserService;
          this.appiservices = APIService;
          this.slug= $scope.$parent.infotest !=undefined ? $scope.$parent.infotest.slug : $scope.ctrl.planet,
          this._$state = $state;
          this.color = "blue";
          this.isFavorited();
      }
    }
    isFavorited(){
      let ctrl = this;
     if (ctrl._User.current) {
      ctrl.appiservices.get_delete(`planets/${this.slug}/favorited`,"Get").then(function(result) {
        console.log(result)
        if(result.favorited){
          ctrl.color ="red"
        }else{
          ctrl.color = "blue"
        }
    })
     }
    }
    submit() {
     let ctrl = this;
     ctrl.appiservices.post_put(`planets/${this.slug}/favorite`,"","Post").then(function(result) {
          console.log(result)
          if(result.favorited){
            ctrl.color ="red"
          }else{
            ctrl.color = "blue"
          }
      })
    }  
  }
  
  let FavoriteBtn= {
    bindings: {
      planet: '='
    },
    transclude: true,
    controller: FavoriteBtnCtrl,
    controllerAs:"ctrl",
    template:`<a ng-click="ctrl.submit()" class="btn  waves-effect waves-light" ng-class='ctrl.color'>
                <i class="material-icons">favorite</i></a><ng-transclude></ng-transclude>`
  };
  
  export default FavoriteBtn;
  