import angular from 'angular';

//style
import './public/slider.css';
import ngMaterial  from './public/materialize/js/materialize';
import ngMdIcons  from './public/materialize/css/materialize.css';
import  './public/toastr/angular_toastr';
import './public/toastr/angular-toastr.css';


// Import our app config files
import appConfig  from './config/app.config';
import 'angular-ui-router';
import 'angular-animate';
import toastr from 'angular-toastr';


// Import our app functionaity
import './layout';
import './components';
import './services';
import './modules/home';
import './modules/contact';
import './modules/products';
import './modules/auth';
import './modules/quiz_up';
import './modules/profile';
import './modules/settings';
import './modules/wiki';


// Create and bootstrap application
const requires = [
  'ui.router',
  'app.layout',
  'app.components',
  'app.services',
  'app.home',
  'app.auth',
  'app.profile',
  'app.settings',
  'app.contact',
  'app.products',
  'app.quiz',
  'app.wiki',
  'ngAnimate',
  toastr
];

// Mount on window for testing
window.app = angular.module('app', requires);
angular.module('app').config(appConfig);


angular.bootstrap(document, ['app'], {
  /* strictDi: true */
});
