const App = require("../config/constants.config");
export default class UserService {
    constructor(APIService,GraphqlService, JWT, $state, $q) {
      'ngInject';
      this.GraphqlService = GraphqlService;
      this.APIService = APIService;
      this._JWT = JWT;
      this._$state = $state;
      this._$q = $q;
  
      this.current = null;
    }
  
    login_quiz_user(query){
      let ctrl = this;
     return ctrl.GraphqlService.get_prisma(query).then(function(result) {
        console.log(result)
        if(result.login !=null){
          window.localStorage.setItem("quizUser",JSON.stringify(result.login));
          return true
        }else{
          return false;
        }
      })
    }
    register_quiz_user(query){
      let ctrl = this;
      return ctrl.GraphqlService.mutation_prisma(query).then(function(result) {
         console.log(result)
         if(result.create_user !=null){
          window.localStorage.setItem("quizUser",JSON.stringify(result.register));
           return true
         }else{
           return false;
         }
       })
    }
    quizUser(){
     return JSON.parse(window.localStorage.getItem("quizUser"));
    }
    logOut_quiz_user(){
      window.localStorage.removeItem("quizUser");
      window.location.reload();
    }
    attemptAuth(type, credentials) {
      let route="";
      console.log(type)
      if(type == 'login'){ route = "users/login";}else if (type == 'sociallogin') {route = "users/sociallogin"
      } else { route ='users/' }
      console.log(route)
      return this.APIService.post_put(route,credentials,"POST").then(
        (res) => {
          if(!res.errors){
            console.log("loguin man")
            this._JWT.jwt_save(res.token);
            this.current = res;
            return res; 
          }else{
            return res; 
          }
        },
        (err) => {return err}
      );
    };
    update(fields) {
      return this.APIService.post_put('/user',fields,"PUT").then(
        (res) => {
          this.current = res;
          return res; 
        },
        (err) => {return err}
      );
    }
    delete(){
      return this.APIService.get_delete('/user',"DELETE").then(
        (res) => {
          this.current = null;
      this._JWT.jwt_destroy();
      this._$state.go("app.home", { reload: true });
          return true;
        },
        (err) => {return err}
      );
    }
    logout() {
      this.current = null;
      this._JWT.jwt_destroy();
      this._$state.go("app.home", { reload: true });
    } 
    verifyAuth() {
      let deferred = this._$q.defer();
      // check for JWT token
      if (!this._JWT.jwt_get()) {
        deferred.resolve(false);
        return deferred.promise;
      }
  
      if (this.current) {
        deferred.resolve(true);
  
      } else {
        this.APIService.get_delete_headers("/user","GET",  this._JWT.jwt_get()).then(
          (res) => {
            this.current = res;
            deferred.resolve(true);
          },
  
          (err) => {
            this._JWT.jwt_destroy();
            deferred.resolve(false);
          }
        )
      }
  
      return deferred.promise;
    }
    ensureAuthIs(bool) {
      let deferred = this._$q.defer();
  
      this.verifyAuth().then((authValid) => {
        if (authValid !== bool) {
          this._$state.go('app.home')
          deferred.resolve(false);
        } else {
          deferred.resolve(true);
        }
  
      });
  
      return deferred.promise;
    }
  
  }