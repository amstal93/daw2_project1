export const poducts_query = {
    get_all: `{
        all_products {
          product_id
          name
          description
          image
        }
      }`,
    get_product_by_productId: function(product_id) {
      return `
        {
          one_product(product_id:${parseInt(product_id)}){
            name
            description
            image
            shop{
              id
              name
              image
            }
          }
        }
      `
    }

  };