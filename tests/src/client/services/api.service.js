const App = require("../config/constants.config");
export default class APIService {
    constructor( $http,$q) {
      'ngInject';
      this._$http = $http;
      this._$q = $q;
    }
    post_put(url,data,method) {
      return this._$http({
        url: `${App.AppConstants.api}/${url}`,
        method: method,
        data: { data: data }
      })
      .then((res) => res.data.data)
      .catch ((err) => err.data) 
    }
    get_delete(url,method) {
        let deferred = this._$q.defer();
        this._$http({
          url: `${App.AppConstants.api}/${url}`,
          method: method
        })
        .then((res) => deferred.resolve(res.data.data))
        .catch ((err) =>  deferred.reject(err.data.data));
        return deferred.promise; 
      }
    get_delete_headers(url,method,header){
      let deferred = this._$q.defer();
      this._$http({
        url: `${App.AppConstants.api}/${url}`,
        method: method,
        headers: {Authorization: 'Token ' + header}
      })
      .then((res) => deferred.resolve(res.data.data))
      .catch ((err) =>  deferred.reject(err));
      return deferred.promise; 
    }
    get_from_moleculer(url,method,domain){
      let deferred = this._$q.defer();
        this._$http({
          url: `${App.AppConstants.moleculer(domain)}${url}`,
          method: method
        })
        .then((res) => deferred.resolve(res.data))
        .catch ((err) =>  deferred.reject(err.data));
        return deferred.promise; 
    }
  
  }