'use strict'

const DbService = require("../mixins/db.mixin");

module.exports = {
  name: 'eras',
  mixins: [DbService("eras")],

  settings: {
    fields: ['id', 'name', 'description',"image"]
  },

  actions: {

    get: {
      params: {
        id: 'string'
      },
      handler(ctx) {
				console.log('****ctx.params get*****');
        		console.log(ctx.params.id);
        
				return this.adapter.findById(ctx.params.id )
					.then(entity => {
						console.log('****entity get*****');
						console.log(entity);
						return entity;
					})
			}
    },	

    getAll: {
      handler(ctx) {
				console.log('****ctx.params*****');
				console.log(ctx.params);

				return this.adapter.find()
					.then(entity => {
						console.log('****entity*****');
						console.log(entity);
						return entity;
					})
			}
    },
  },

  methods: {
    /* findByID(id) {
		console.log(id)
	  return this.adapter.findOne({ "_id": id });
    }, */
    
		seedDB() {
			this.logger.info("Seed Items DB...");
			return Promise.resolve()
			.then(() => this.adapter.insert({
				era_id:"1",
				name: "Era Pre-República",
				description:"La Era Pre-República (—25.053 ABY), también conocida como la Edad Temprana Hyperspacial, fue el período anterior a la formación de la República Galáctica.",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/a/ab/ConstructionOfTheStarForge.JPG/revision/latest/scale-to-width-down/350?cb=20070704094245"
			}))
			.then(() => this.adapter.insert({
				era_id:"2",
				name: "Era de la Antigua República",
				description:"La Era de la Antigua República, también conocida como la Era de los Sith, tiene lugar entre la fundación de la Antigua República en el 25.053 ABY y la Séptima Batalla de Ruusan en el 1.000 ABY",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/9/97/Era-old.png/revision/latest?cb=20060623172445"
			}))
			.then(() => this.adapter.insert({
				era_id:"3",
				name: "Era del Alzamiento del Imperio",
				description:"La Era del Alzamiento del Imperio tiene lugar entre 1.000 ABY (Séptima Batalla de Ruusan) y 0 ABY (Batalla de Toprawa) poco antes de la Batalla de Yavin e Inicio de la Guerra Civil Galáctica.Los eventos sucedidos en La Amenaza Fantasma, El Ataque de los Clones y La Venganza de los Sith tienen lugar durante esta era. Los eventos de The Clone Wars y Star Wars Rebels también tienen lugar en esta era. ",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/2/29/Era-imp.png/revision/latest?cb=20060623172532"
			}))
			.then(() => this.adapter.insert({
				era_id:"4",
				name: "Era de la Rebelión",
				description:"La Era de la Rebelión, también conocida como era clásica, toma lugar durante la Guerra Civil Galáctica en 0 ABY hasta un año después de la caída del Imperio Galáctico (5 DBY).Los hechos ocurridos durante Una Nueva Esperanza, El Imperio Contraataca y El Retorno del Jedi, tienen lugar en esta era. ",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/e/e7/Era-reb.png/revision/latest?cb=20060623172548"
			}))
			
    	},
	},

	afterConnected() {
		return this.adapter.count().then(count => {
			if (count == 0) {
				this.seedDB();
			}
		});
	}

}
