const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const Users = require('../models/Users');
const User = mongoose.model('User');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
   console.log(`id: ${id}`);
  User.findById(id)
    .then(user => {
      /* console.log(user) */
      done(null, user);
    })
    .catch(error => {
      console.log(`Error: ${error}`);
    });
});

passport.use(new LocalStrategy({
  usernameField: 'data[email]',
  passwordField: 'data[password]'
}, function(email, password, done) {
  User.findOne({email: email}).then(function(user){
    if(!user || !user.validPassword(password)){
      return done(null, false, {errors: {'email or password': 'is invalid'}});
    }

    return done(null, user);
  }).catch(done);
}));
passport.use(new GoogleStrategy({
  clientID: "755291336779-g5ovp37di833icq353ajqrks38ldjki4.apps.googleusercontent.com",
  clientSecret: "DxMIy4nkgXgu0jNUTHJCa-fw",
  callbackURL: "http://localhost:3002/api/auth/google/callback",
  passReqToCallback: true,
  scope: ["email","profile"]
  },
  function(request, accessToken, refreshToken,profile, done) {
    User.findOne({ 'idsocial' : profile.id }, function(err, user) {
        if (err){
          return done(err);
        }
        // if the user is found then log them in
        if (user) {
            return done(null, user);
        } else {
          let user = new User({
              idsocial: profile.id,
              username: profile.displayName,
              email: profile.emails[0].value,
              image: profile.photos[0].value,
          });

          user.save(function(err) {
              if(err){
                  return done(null, user);
              }
              return done(null, user);
          });
      }
    });
  } 
));