let mongoose = require('mongoose');
let router = require('express').Router();
var passport = require('passport');
const Users = require('../../models/Users');
let User = mongoose.model('User');
const Planets = require('../../models/Planets');
const PlanetDb = mongoose.model("Planets");
let auth = require('../auth');

/* Create user */
router.post('/users', function(req, res, next){ 
    let user = new User();
    let info = req.body.data;
    user.username = info.username;
    user.email = info.email;
    console.log("estas pero no deverias");
    user.setPassword(info.password);
    console.log("estoy aqui 2")
    user.save().then(function(){
      return res.json({data: user.toAuthJSON()});
    }).catch(next);
});
/* login user */
router.post('/users/login', function(req, res, next){
   passport.authenticate('local', {session: false}, function(err, user, info){

       if(err){ return next(err); }
       if(user){
        user.token = user.generateJWT();
        return res.json({data: user.toAuthJSON()});
      } else {
        return res.status(422).json(info);
      }
    })(req, res, next);
});
/* update user */
router.put('/user', auth.required, function(req, res, next){
    let info = req.body.data;
    console.log(info);
    User.findById(req.payload.id).then(function(user){
      if(!user){ return res.sendStatus(401); }
       for (const key in info) {
           if (typeof info[key] !== undefined) {        
                  user[key]=info[key]; 
           }
       }
      return user.save().then(function(){
        return res.json({data: user.toAuthJSON()});
      }); 
    }).catch(next);
});
/* delete user */
router.delete('/user', auth.required, function(req, res, next){
    User.findOneAndDelete({_id:req.payload.id}).then(function(result){
      if(result!=null){return res.json({data:"ok"})}else{return res.json({data:"Usuario no encontrado"})}
    });
});
/* get user profile */
router.get('/user', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
      if(!user){ return res.sendStatus(401); }
  
      return res.json({data: user.toAuthJSON()});
    }).catch(next);
});
/* social login */
router.post('/users/sociallogin', function(req, res, next){
  let memorystore = req.sessionStore;
  let sessions = memorystore.sessions;
  let sessionUser;
  for(var key in sessions){
    sessionUser = (JSON.parse(sessions[key]).passport.user);
  }

  User.findOne({ '_id' : sessionUser }, function(err, user) {
    /* console.log(err); */
    /* console.log(user); */
    if (err)
      return done(err);
    // if the user is found then log them in
    if (user) {
       /*  console.log(user); */
        user.token = user.generateJWT();
        return res.json({data: user.toAuthJSON()});// user found, return that user
    } else {
        return res.status(422).json(err);
    }
    });
});

router.get('/auth/google',passport.authenticate('google', { scope: ['email',"profile"] }));
router.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    /* console.log("Successful authentication, redirect home.") */
    res.redirect('http://localhost:3002/#!/social_login');
  });

module.exports = router;
 